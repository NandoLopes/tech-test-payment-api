using PaymentAPI.Context;
using Microsoft.AspNetCore.Mvc;
using PaymentAPI.Models;
using PaymentAPI.Dtos;
using Microsoft.EntityFrameworkCore;

namespace PaymentAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendaController : ControllerBase
    {
        public DataContext _context { get; }
        public VendaController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult ObterTodos()
        {
            var Vendas = _context.Vendas
                .Include(x => x.Status)
                .Include(x => x.Vendedor)
                .Include(x => x.VendaItens)
                    .ThenInclude(x => x.Item)
                .ToList();
            return Ok(Vendas);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var Venda = _context.Vendas
                .Include(x => x.Status)
                .Include(x => x.Vendedor)
                .Include(x => x.VendaItens)
                    .ThenInclude(x => x.Item)
                .FirstOrDefault(x => x.Id == id);
            
            if(Venda == null)
                return NotFound();

            return Ok(Venda);
        }

        [HttpGet("PorVendedor/{nome}")]
        public IActionResult ObterPorNomeVendedor(string nome)
        {
            var Venda = _context.Vendas
                .Include(x => x.Status)
                .Include(x => x.Vendedor)
                .Include(x => x.VendaItens)
                    .ThenInclude(x => x.Item)
                .Where(x => x.Vendedor.Nome.Contains(nome));
            
            if(Venda == null)
                return NotFound();

            return Ok(Venda);
        }

        [HttpGet("PorIdVendedor/{nome}")]
        public IActionResult ObterPorVendedorId(int vendedorId)
        {
            var Venda = _context.Vendas
                .Include(x => x.Status)
                .Include(x => x.Vendedor)
                .Include(x => x.VendaItens)
                    .ThenInclude(x => x.Item)
                .Where(x => x.VendedorId == vendedorId);
            
            if(Venda == null)
                return NotFound();

            return Ok(Venda);
        }

        [HttpGet("PorData/{data}")]
        public IActionResult ObterPorData(DateTime data)
        {
            var Venda = _context.Vendas
                .Include(x => x.Status)
                .Include(x => x.Vendedor)
                .Include(x => x.VendaItens)
                    .ThenInclude(x => x.Item)
                .Where(x => x.Data.Date == data.Date);
            
            if(Venda == null)
                return NotFound();

            return Ok(Venda);
        }

        [HttpGet("PorIdStatus/{statusId}")]
        public IActionResult ObterPorStatus(int statusId)
        {
            var Venda = _context.Vendas
                .Include(x => x.Status)
                .Include(x => x.Vendedor)
                .Include(x => x.VendaItens)
                    .ThenInclude(x => x.Item)
                .Where(x => x.StatusId == statusId);
            
            if(Venda == null)
                return NotFound();

            return Ok(Venda);
        }

        [HttpGet("PorIdItens/{itensId}")]
        public IActionResult ObterPorItens(int itensId)
        {
            var Venda = _context.Vendas
                .Include(x => x.Status)
                .Include(x => x.Vendedor)
                .Include(x => x.VendaItens)
                    .ThenInclude(x => x.Item)
                .FirstOrDefault(x => x.VendaItens.Any(x => x.Id == itensId));
            
            if(Venda == null)
                return NotFound();

            return Ok(Venda);
        }

        [HttpPost]
        public IActionResult Criar(PostVendaDto venda)
        {
            if(venda.VendedorId <= 0)
                return BadRequest(new { Erro = "É necessário um vendedor válido." });
            
            
            if(!venda.Itens.Any() || venda.Itens?.Count == 0)
                return BadRequest(new { Erro = "É necessário ao menos 1 item." });

            var vendedor = _context.Vendedores.Find(venda.VendedorId);
            if(vendedor == null)
                return BadRequest(new { Erro = "É necessário um vendedor válido." });

            List<VendaItem> vendaItens = new List<VendaItem>();
            foreach (var item in venda.Itens)
            {
                var newItem = _context.Itens.Find(item.ItemId);
                if(newItem == null)
                    return BadRequest(new { Erro = $"Item {item.ItemId} não existe." });
                
                VendaItem newItemVemda = new VendaItem { 
                    Item = newItem,
                    QuantidadeVendida = item.Quantidade,
                    ValorItemVendido = newItem.Valor,
                    ValorTotal = newItem.Valor * item.Quantidade
                };
                vendaItens.Add(newItemVemda);
            }

            var status = _context.Status.Find(1);
            
            decimal valorTotal = 0;
            foreach (var item in vendaItens){
                valorTotal += item.ValorTotal;
            }

            Venda novaVenda = new Venda{
                VendedorId = venda.VendedorId,
                Vendedor = vendedor,
                Data = DateTime.Now,
                Status = status,
                VendaItens = vendaItens,
                ValorVenda = valorTotal
            };

            _context.Vendas.Add(novaVenda);
            _context.SaveChanges();
            return Ok();
        }

        [HttpPut]
        public IActionResult AtualizarVenda(int vendaId, int novoStatusId){
            if(vendaId <= 0 || novoStatusId <= 0)
                return BadRequest(new { Erro = "Id da venda e do novo status são obrigatórios."});

            Venda venda = _context.Vendas.Find(vendaId);
            if(venda == null)
                return NotFound();

            var statusPossiveis = _context.StatusTransicao
                .Include(x => x.PossivelTransicao)
                .Where(x => x.StatusAtualId == venda.StatusId)
                .Select(x => x.PossivelTransicao).ToList();

            Status novoStatus = statusPossiveis.FirstOrDefault(x => x.Id == novoStatusId);

            if(novoStatus == null)
                return BadRequest(new { Erro = "Status inválido."});

            venda.Status = novoStatus;

            _context.Vendas.Update(venda);
            _context.SaveChanges();

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var VendaBanco = _context.Vendas.Find(id);

            if (VendaBanco == null)
                return NotFound();

            _context.Vendas.Remove(VendaBanco);
            _context.SaveChanges();
            return NoContent();
        }
    }
}