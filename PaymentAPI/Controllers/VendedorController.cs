using PaymentAPI.Context;
using Microsoft.AspNetCore.Mvc;
using PaymentAPI.Models;

namespace PaymentAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendedorController : ControllerBase
    {
        public DataContext _context { get; }
        public VendedorController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult ObterTodos()
        {
            var vendedores = _context.Vendedores.ToList();
            return Ok(vendedores);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var vendedor = _context.Vendedores.Find(id);
            
            if(vendedor == null)
                return NotFound();

            return Ok(vendedor);
        }

        [HttpGet("PorNome/{nome}")]
        public IActionResult ObterPorNome(string nome)
        {
            var vendedor = _context.Vendedores.Where(x => x.Nome.Contains(nome));
            
            if(vendedor == null)
                return NotFound();

            return Ok(vendedor);
        }

        [HttpGet("ObterPorCpf/{cpf}")]
        public IActionResult ObterPorCpf(string cpf)
        {
            var vendedor = _context.Vendedores.Where(x => x.Cpf == cpf);
            
            if(vendedor == null)
                return NotFound();

            return Ok(vendedor);
        }

        [HttpGet("ObterPorEmail/{email}")]
        public IActionResult ObterPorEmail(string email)
        {
            var vendedor = _context.Vendedores.Where(x => x.Email == email);
            
            if(vendedor == null)
                return NotFound();

            return Ok(vendedor);
        }

        [HttpPost]
        public IActionResult Criar(Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { id = vendedor.Id }, vendedor);
        }
        
        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Vendedor vendedor)
        {
            var vendedorBanco = _context.Vendedores.Find(id);

            if (vendedorBanco == null)
                return NotFound();

            vendedorBanco.Cpf = vendedor.Cpf;
            vendedorBanco.Nome = vendedor.Nome;
            vendedorBanco.Email = vendedor.Email;
            vendedorBanco.Telefone = vendedor.Telefone;

            _context.Vendedores.Update(vendedorBanco);
            _context.SaveChanges();

            return Ok(vendedorBanco);
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var vendedorBanco = _context.Vendedores.Find(id);

            if (vendedorBanco == null)
                return NotFound();

            _context.Vendedores.Remove(vendedorBanco);
            _context.SaveChanges();
            return NoContent();
        }
    }
}