using PaymentAPI.Context;
using Microsoft.AspNetCore.Mvc;
using PaymentAPI.Models;

namespace PaymentAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ItemController : ControllerBase
    {
        public DataContext _context { get; }
        public ItemController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult ObterTodos()
        {
            var Itens = _context.Itens.ToList();
            return Ok(Itens);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var Item = _context.Itens.Find(id);
            
            if(Item == null)
                return NotFound();

            return Ok(Item);
        }

        [HttpGet("PorNome/{nome}")]
        public IActionResult ObterPorNome(string nome)
        {
            var Item = _context.Itens.Where(x => x.Nome.Contains(nome));
            
            if(Item == null)
                return NotFound();

            return Ok(Item);
        }

        [HttpPost]
        public IActionResult Criar(Item Item)
        {
            _context.Add(Item);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { id = Item.Id }, Item);
        }
        
        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Item Item)
        {
            var ItemBanco = _context.Itens.Find(id);

            if (ItemBanco == null)
                return NotFound();

            ItemBanco.Nome = Item.Nome;
            ItemBanco.Valor = Item.Valor;

            _context.Itens.Update(ItemBanco);
            _context.SaveChanges();

            return Ok(ItemBanco);
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var ItemBanco = _context.Itens.Find(id);

            if (ItemBanco == null)
                return NotFound();

            _context.Itens.Remove(ItemBanco);
            _context.SaveChanges();
            return NoContent();
        }
    }
}