namespace PaymentAPI.Dtos
{
    public class PostVendaDto
    {
        public int VendedorId { get; set; }
        public List<PostVendaItemDto> Itens { get; set; }      
    }
}