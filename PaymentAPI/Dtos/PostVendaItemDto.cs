namespace PaymentAPI.Dtos
{
    public class PostVendaItemDto
    {
        public int Quantidade { get; set; }
        public int ItemId { get; set; }
    }
}