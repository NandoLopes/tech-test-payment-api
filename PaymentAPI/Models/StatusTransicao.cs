namespace PaymentAPI.Models
{
    public class StatusTransicao
    {
        public int? StatusAtualId { get; set; }
        public Status StatusAtual { get; set; }
        public int? PossivelTransicaoId { get; set; }
        public Status PossivelTransicao { get; set; }
    }
}