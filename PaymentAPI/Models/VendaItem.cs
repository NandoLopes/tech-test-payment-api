namespace PaymentAPI.Models
{
    public class VendaItem
    {
        public int Id { get; set; }
        public int QuantidadeVendida { get; set; }
        public decimal ValorItemVendido { get; set; }
        public decimal ValorTotal { get; set; }
        public int VendaId { get; set; }        
        public Venda Venda { get; set; }
        public int ItemId { get; set; }
        public Item Item { get; set; }
    }
}