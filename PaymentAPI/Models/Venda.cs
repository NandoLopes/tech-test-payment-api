namespace PaymentAPI.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public int VendedorId { get; set; }
        public Vendedor Vendedor { get; set; }
        public DateTime Data { get; set; }
        public int StatusId { get; set; }
        public Status Status { get; set; }
        public List<VendaItem> VendaItens { get; set; }
        public decimal ValorVenda { get; set; }
    }
}