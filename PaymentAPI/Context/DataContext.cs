using Microsoft.EntityFrameworkCore;
using PaymentAPI.Models;

namespace PaymentAPI.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options) { }

        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Item> Itens { get; set; }
        public DbSet<VendaItem> VendaItem { get; set; }
        public DbSet<Status> Status { get; set; }
        public DbSet<StatusTransicao> StatusTransicao { get; set; }

        protected override void OnModelCreating(ModelBuilder builder){
            var decimalProps = builder.Model
                .GetEntityTypes()
                .SelectMany(t => t.GetProperties())
                .Where(p => (System.Nullable.GetUnderlyingType(p.ClrType) ?? p.ClrType) == typeof(decimal));

            foreach (var property in decimalProps)
            {
                property.SetPrecision(18);
                property.SetScale(2);
            }

            builder.Entity<StatusTransicao>()
                .HasKey(x => new {x.StatusAtualId, x.PossivelTransicaoId});

            builder.Entity<StatusTransicao>()
                .HasOne(x => x.StatusAtual)
                .WithMany()
                .OnDelete(DeleteBehavior.NoAction);
            
            builder.Entity<StatusTransicao>()
                .HasOne(x => x.PossivelTransicao)
                .WithMany()
                .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<VendaItem>()
                .HasOne(x => x.Item)
                .WithMany();

            builder.Entity<VendaItem>()
                .HasOne(x => x.Venda)
                .WithMany();

            builder.Entity<Venda>()
                .HasOne(x => x.Status)
                .WithMany();
            builder.Entity<Venda>()
                .HasOne(x => x.Vendedor)
                .WithMany();
            builder.Entity<Venda>()
                .HasMany(x => x.VendaItens)
                .WithOne(x => x.Venda);

            //Status Seed
            builder.Entity<Status>().HasData(
                new Status{
                    Id = 1, Descricao = "Aguardando pagamento"
                },
                new Status{
                    Id = 2, Descricao = "Pagamento aprovado"
                },
                new Status{
                    Id = 3, Descricao = "Enviado para transportadora"
                },
                new Status{
                    Id = 4, Descricao = "Entregue"
                },
                new Status{
                    Id = 5, Descricao = "Cancelada"
                }
            );

            builder.Entity<StatusTransicao>().HasData(
                new StatusTransicao{
                    StatusAtualId = 1, PossivelTransicaoId = 2
                },
                new StatusTransicao{
                    StatusAtualId = 1, PossivelTransicaoId = 5
                },
                new StatusTransicao{
                    StatusAtualId = 2, PossivelTransicaoId = 3
                },
                new StatusTransicao{
                    StatusAtualId = 2, PossivelTransicaoId = 5
                },
                new StatusTransicao{
                    StatusAtualId = 3, PossivelTransicaoId = 4
                }
            );
        }

    }
}